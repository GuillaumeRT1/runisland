import { Component } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { AuthSignin } from "../models";
import { Kinvey } from "kinvey-nativescript-sdk";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  host: { class: "handler" },
  template: `
    <FlexboxLayout
      backgroundColor="#d3d3d3"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
    >
      <Image src="~/images/logo.png" stretch="none" class="logo"></Image>
      <StackLayout>
        <RadDataForm [source]="user">
          <TKEntityProperty
            index="0"
            tkDataFormProperty
            name="username"
            displayName="Adresse Mail"
            [required]="true"
          >
            <TKPropertyEditor tkEntityPropertyEditor type="Email">
            </TKPropertyEditor>
            <TKNonEmptyValidator
              tkEntityPropertyValidators
              errorMessage="Ce champ est requis."
            ></TKNonEmptyValidator>
            <TKEmailValidator
              tkEntityPropertyValidators
              errorMessage="Veuillez saisir un courriel valide."
            ></TKEmailValidator>
          </TKEntityProperty>
          <TKEntityProperty
            index="1"
            tkDataFormProperty
            name="password"
            displayName="Mot de passe"
            [required]="true"
          >
            <TKPropertyEditor
              tkEntityPropertyEditor
              type="Password"
            ></TKPropertyEditor>
            <TKNonEmptyValidator
              tkEntityPropertyValidators
              errorMessage="Ce champ est requis."
            ></TKNonEmptyValidator>
          </TKEntityProperty>
        </RadDataForm>
      </StackLayout>
      <FlexboxLayout justifyContent="center" alignItems="center">
        <Button text="Connexion" (tap)="onSubmit()"></Button>
        <Button text="Créer un compte" (tap)="signUp()"></Button>
      </FlexboxLayout>
    </FlexboxLayout>
  `,
  styles: [``]
})
export class AppPublicHandler {
  readonly languageURL = "/language";
  readonly homeURL = "/home";
  readonly SignUpURL = "/signup";
  user: AuthSignin = { username: "", password: "" };

  constructor(readonly page: Page, readonly router: RouterExtensions) {
    this.page.actionBarHidden = true;
  }
  async isAuthenticated(): Promise<boolean> {
    try {
      await Kinvey.User.getActiveUser().me();
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }
  ngOnInit() {
    Kinvey.ping()
      .then(response => {
        console.log(
          `Kinvey Ping Success. Kinvey Service is alive, version: ${
            response.version
          }, response: ${response.kinvey}`
        );
      })
      .catch(error => {
        console.log(`Kinvey Ping Failed. Response: ${error.description}`);
      });
  }
  async onSubmit() {
    const isAuthenticated: boolean = await this.isAuthenticated();
    if (isAuthenticated) {
      this.router.navigateByUrl(this.homeURL, { clearHistory: true });
    } else {
      try {
        await Kinvey.User.login(this.user.username, this.user.password);
        this.router.navigateByUrl(this.languageURL, { clearHistory: true });
      } catch (error) {
        console.error(error);
        alert("Données Invalides");
      }
    }
  }
  signUp(){
    this.router.navigateByUrl(this.SignUpURL, { clearHistory: true });
  }
}
