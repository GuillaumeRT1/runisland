import { Component } from "@angular/core";
import { Kinvey } from "kinvey-nativescript-sdk";
import { Language } from "../models";
import { Page } from "tns-core-modules/ui/page/page";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  host: { class: "handler" },
  template: `
    <GridLayout rows="*,auto">
      <ListView
        rowspan="2"
        [items]="langues"
        (itemTap)="onItemTap($event)"
        class="list-group"
      >
        <ng-template let-langue="item" let-even="even">
          <StackLayout
            orientation="horizontal"
            class="list-group-item"
            [ngClass]="{ 'bg-light': even }"
          >
            <Label [text]="langue.nom"></Label>
          </StackLayout>
        </ng-template>
      </ListView>
    </GridLayout>
  `,
  styles: [``]
})
export class AppLanguageHandler {
  langues: Language["name"];
  productsStore: Kinvey.SyncStore = Kinvey.DataStore.collection("Language");
  readonly PublicURL = "/connexion";
  constructor(readonly page: Page, readonly router: RouterExtensions) {
    this.page.actionBarHidden = true;
  }

  ngOnInit() {
    this.readMany().subscribe(
      langues => {
        this.langues = langues;
      },
      (error: Kinvey.BaseError) => alert(error)
    );
  }
  readMany(): any {
    const query = new Kinvey.Query();
    query.ascending("nom");
    return this.productsStore.find(query);
  }
  onItemTap({ index }: { index: number }) {
    this.router.navigateByUrl("/home");
  }
}
