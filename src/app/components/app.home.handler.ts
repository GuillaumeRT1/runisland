import { Component } from "@angular/core";
import { Kinvey } from "kinvey-nativescript-sdk";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  host: { class: "handler" },
  template: `
    <StackLayout>
      <Label text=" Bonjour, et Bienvenue sur Run'Island !"></Label>
      <Button text="Déconnexion" (tap)="signout()"></Button>
    </StackLayout>
  `,
  styles: [``]
})
export class AppHomeHandler {
  readonly authURL = "";
  constructor(readonly router: RouterExtensions) {}
  async signout() {
    try {
      await Kinvey.User.logout();
      this.router.navigateByUrl(this.authURL, { clearHistory: true });
    } catch (error) {
      console.error(error);
    }
  }
}
