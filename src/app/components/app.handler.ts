import { Component } from '@angular/core';

@Component({
  selector: 'app-handler',
  template: `<page-router-outlet></page-router-outlet>`,
  styles: [``],
})
export class AppHandler {}