export interface AuthSignin {
    username: string;
    password: string;
  }
  export interface Language {
  name: string;
  }
  
  export interface LanguagesResponse {
    data: Language[];
  }