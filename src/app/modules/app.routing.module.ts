import { NgModule, Component } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AppHomeHandler } from '../components/app.home.handler';
import { AppAskHandler } from '../components/app.ask.handler';
import { AppPublicHandler } from '../components/app.public.handler';
import { AppSignupHandler } from '../components/app.signup.handler';
import { AppLanguageHandler } from '../components/app.language.handler';


const routes: Routes = [
  {
    path: 'language',
    component: AppLanguageHandler
  },
  {
    path: 'home',
    component: AppHomeHandler
  },
  {
    path: 'ask',
    component: AppAskHandler
  },
  {
    path: 'signup',
    component: AppSignupHandler
  },
  {
    path: '',
    component: AppPublicHandler
  }
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule],
})
export class AppRoutingModule {}