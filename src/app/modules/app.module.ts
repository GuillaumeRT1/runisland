import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptAnimationsModule } from "nativescript-angular/animations";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

import { AppHandler } from "../components/app.handler";
import { AppRoutingModule } from "./app.routing.module";
import { AppPlaceComponents } from "../components/app.place.components";
import { AppHelpComponents } from "../components/app.help.components";
import { AppHomeHandler } from "../components/app.home.handler";
import { AppAskHandler } from "../components/app.ask.handler";
import { AppPublicHandler } from "../components/app.public.handler";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";
import { AppSignupHandler } from "../components/app.signup.handler";
import { AppLanguageHandler } from "../components/app.language.handler";

@NgModule({
  bootstrap: [AppHandler],
  declarations: [
    AppHandler,
    AppPlaceComponents,
    AppHelpComponents,
    AppHomeHandler,
    AppAskHandler,
    AppPublicHandler,
    AppSignupHandler,
    AppLanguageHandler
  ],
  imports: [
    NativeScriptModule,
    NativeScriptAnimationsModule,
    NativeScriptHttpClientModule,
    AppRoutingModule,
    NativeScriptUIDataFormModule
    ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
